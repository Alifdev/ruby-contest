class ChangeContentToQuestions < ActiveRecord::Migration[5.0]
  def change
    change_column :questions, :content, :string
  end
end
