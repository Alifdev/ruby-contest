class ChangeContentToAnswers < ActiveRecord::Migration[5.0]
  def change
    change_column :answers, :content, :string
  end
end
