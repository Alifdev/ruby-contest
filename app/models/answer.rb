# == Schema Information
#
# Table name: answers
#
#  id             :integer          not null, primary key
#  content        :string
#  correct_answer :boolean
#  question_id    :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#
# Indexes
#
#  index_answers_on_question_id  (question_id)
#
# Foreign Keys
#
#  fk_rails_3d5ed4418f  (question_id => questions.id)
#

class Answer < ApplicationRecord
  belongs_to :question
end
