# == Schema Information
#
# Table name: questions
#
#  id         :integer          not null, primary key
#  content    :string
#  quiz_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_questions_on_quiz_id  (quiz_id)
#
# Foreign Keys
#
#  fk_rails_0238c45a86  (quiz_id => quizzes.id)
#

class Question < ApplicationRecord
  belongs_to :quiz
  has_many :answers, dependent: :destroy
  accepts_nested_attributes_for :answers, reject_if: :all_blank, allow_destroy: true

  def check_answer(answer_id)
    answers.where(correct_answer: true, id: :answer_id).any?
  end
end
