class QuizzesController < ApplicationController
  before_action :all_quiz, only: [:index]
  before_action :find_quiz, only: [:start_contest, :show, :edit, :update, :destroy]

  def new
    @quiz = Quiz.new
    @quiz.questions.build
  end

  def create
    @quiz = Quiz.create(quiz_params)

    respond_to do |format|
      if @quiz.save
        format.html { redirect_to root_path }
        format.json { render :show, status: :created, location: @quiz }
      else
        format.html { render :new }
        format.json { render json: @quiz.errors, status: :unprocessable_entity }
      end
    end
  end

  def show
    @quiz_question = @quiz.questions.order("RANDOM()").all
  end

  def update
    @quiz.update_attributes(quiz_params)
    redirect_to @quiz
  end

  def destroy
    @quiz.destroy
    redirect_to @quiz
  end

  private

  def all_quiz
    @quizzes = Quiz.all
  end

  def find_quiz
    @quiz = Quiz.find(params[:id])
  end

  def quiz_params
    params.require(:quiz).permit(:name, questions_attributes: [:id, :content, :_destroy, answers_attributes: [:id, :content, :correct_answer, :_destroy]])
  end
end
